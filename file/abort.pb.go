// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.30.0
// 	protoc        v3.21.9
// source: com/ruijc/storage/core/file/abort.proto

package file

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 取消分块上传请求
type AbortReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 编号
	Id uint64 `protobuf:"varint,3,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *AbortReq) Reset() {
	*x = AbortReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_com_ruijc_storage_core_file_abort_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AbortReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AbortReq) ProtoMessage() {}

func (x *AbortReq) ProtoReflect() protoreflect.Message {
	mi := &file_com_ruijc_storage_core_file_abort_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AbortReq.ProtoReflect.Descriptor instead.
func (*AbortReq) Descriptor() ([]byte, []int) {
	return file_com_ruijc_storage_core_file_abort_proto_rawDescGZIP(), []int{0}
}

func (x *AbortReq) GetId() uint64 {
	if x != nil {
		return x.Id
	}
	return 0
}

// 取消分块上传返回
type AbortRsp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 编号
	Id uint64 `protobuf:"varint,3,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *AbortRsp) Reset() {
	*x = AbortRsp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_com_ruijc_storage_core_file_abort_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AbortRsp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AbortRsp) ProtoMessage() {}

func (x *AbortRsp) ProtoReflect() protoreflect.Message {
	mi := &file_com_ruijc_storage_core_file_abort_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AbortRsp.ProtoReflect.Descriptor instead.
func (*AbortRsp) Descriptor() ([]byte, []int) {
	return file_com_ruijc_storage_core_file_abort_proto_rawDescGZIP(), []int{1}
}

func (x *AbortRsp) GetId() uint64 {
	if x != nil {
		return x.Id
	}
	return 0
}

var File_com_ruijc_storage_core_file_abort_proto protoreflect.FileDescriptor

var file_com_ruijc_storage_core_file_abort_proto_rawDesc = []byte{
	0x0a, 0x27, 0x63, 0x6f, 0x6d, 0x2f, 0x72, 0x75, 0x69, 0x6a, 0x63, 0x2f, 0x73, 0x74, 0x6f, 0x72,
	0x61, 0x67, 0x65, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x2f, 0x61, 0x62,
	0x6f, 0x72, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1b, 0x63, 0x6f, 0x6d, 0x2e, 0x72,
	0x75, 0x69, 0x6a, 0x63, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x2e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x66, 0x69, 0x6c, 0x65, 0x22, 0x1a, 0x0a, 0x08, 0x41, 0x62, 0x6f, 0x72, 0x74, 0x52,
	0x65, 0x71, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x04, 0x52, 0x02,
	0x69, 0x64, 0x22, 0x1a, 0x0a, 0x08, 0x41, 0x62, 0x6f, 0x72, 0x74, 0x52, 0x73, 0x70, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x04, 0x52, 0x02, 0x69, 0x64, 0x42, 0x48,
	0x0a, 0x1b, 0x63, 0x6f, 0x6d, 0x2e, 0x72, 0x75, 0x69, 0x6a, 0x63, 0x2e, 0x73, 0x74, 0x6f, 0x72,
	0x61, 0x67, 0x65, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x66, 0x69, 0x6c, 0x65, 0x50, 0x01, 0x5a,
	0x27, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x72, 0x75, 0x69, 0x6a,
	0x63, 0x2f, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x66,
	0x69, 0x6c, 0x65, 0x3b, 0x66, 0x69, 0x6c, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_com_ruijc_storage_core_file_abort_proto_rawDescOnce sync.Once
	file_com_ruijc_storage_core_file_abort_proto_rawDescData = file_com_ruijc_storage_core_file_abort_proto_rawDesc
)

func file_com_ruijc_storage_core_file_abort_proto_rawDescGZIP() []byte {
	file_com_ruijc_storage_core_file_abort_proto_rawDescOnce.Do(func() {
		file_com_ruijc_storage_core_file_abort_proto_rawDescData = protoimpl.X.CompressGZIP(file_com_ruijc_storage_core_file_abort_proto_rawDescData)
	})
	return file_com_ruijc_storage_core_file_abort_proto_rawDescData
}

var file_com_ruijc_storage_core_file_abort_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_com_ruijc_storage_core_file_abort_proto_goTypes = []interface{}{
	(*AbortReq)(nil), // 0: com.ruijc.storage.core.file.AbortReq
	(*AbortRsp)(nil), // 1: com.ruijc.storage.core.file.AbortRsp
}
var file_com_ruijc_storage_core_file_abort_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_com_ruijc_storage_core_file_abort_proto_init() }
func file_com_ruijc_storage_core_file_abort_proto_init() {
	if File_com_ruijc_storage_core_file_abort_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_com_ruijc_storage_core_file_abort_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AbortReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_com_ruijc_storage_core_file_abort_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AbortRsp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_com_ruijc_storage_core_file_abort_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_com_ruijc_storage_core_file_abort_proto_goTypes,
		DependencyIndexes: file_com_ruijc_storage_core_file_abort_proto_depIdxs,
		MessageInfos:      file_com_ruijc_storage_core_file_abort_proto_msgTypes,
	}.Build()
	File_com_ruijc_storage_core_file_abort_proto = out.File
	file_com_ruijc_storage_core_file_abort_proto_rawDesc = nil
	file_com_ruijc_storage_core_file_abort_proto_goTypes = nil
	file_com_ruijc_storage_core_file_abort_proto_depIdxs = nil
}
